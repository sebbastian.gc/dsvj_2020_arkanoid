#ifndef MENU_H
#define MENU_H

#include "raylib.h"
#include "game/objects/button/button.h"

namespace arkanoid {

	namespace menu {		
		extern button* back;

		void init();		
		void update();				
		void draw();		
		void deinit();
	}
}
#endif