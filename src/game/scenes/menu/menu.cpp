#include "menu.h"

#include "game/global.h"
#include "game/input/input.h"

using namespace arkanoid;

SCREENS   arkanoid::currentScreen;
button*	  menu::back;

namespace arkanoid {
	namespace menu {

		Texture2D screenMenuTexture;
		Texture2D screenCreditsTexture;
		Texture2D screenInstTexture;
		Texture2D playTexture;
		Texture2D instrucTexture;
		Texture2D creditsTexture;
		Texture2D exitTexture;
		Texture2D backTexture;

		button* play;
		button* instructions;
		button* credits;
		button* exit;

		void static  playButton() {
			play->update();
			if (play->getButtonAction()) {
				currentScene = SCENES::GAMEPLAY;
				currentScreen = SCREENS::LV_ONE;
			}
		}
		void static  instructionsButton() {
			instructions->update();
			if (instructions->getButtonAction()) {
				currentScreen = SCREENS::INSTRUCTIONS;
			}
		}
		void static  creditsButton() {
			credits->update();
			if (credits->getButtonAction()) {
				currentScreen = SCREENS::CREDITS;
			}
		}
		void static  exitButton() {
			exit->update();
			if (exit->getButtonAction()) {
				CloseWindow();
			}
		}
		void static  drawMenu() {
			DrawTexture(screenMenuTexture, 0, 0, WHITE);
			play->draw();
			instructions->draw();
			credits->draw();
			exit->draw();
		}

		void init() {
			currentScreen = SCREENS::MENU;
			currentScene = SCENES::MENU;
			screenMenuTexture = LoadTexture("res/assets/menu.png");
			screenCreditsTexture = LoadTexture("res/assets/credits.png");
			screenInstTexture = LoadTexture("res/assets/instructions.png");

			playTexture = LoadTexture("res/assets/botones/play.png");
			instrucTexture = LoadTexture("res/assets/botones/Instructions.png");
			creditsTexture = LoadTexture("res/assets/botones/Credits.png");
			exitTexture = LoadTexture("res/assets/botones/Exit.png");
			backTexture = LoadTexture("res/assets/botones/back.png");

			play = new button(playTexture, -50);
			instructions = new button(instrucTexture, 50);
			credits = new button(creditsTexture, 150);
			exit = new button(exitTexture, 250);
			back = new button(backTexture, 250, 50);
		}
		void update() {
			switch (currentScreen) {
			case SCREENS::MENU:
				playButton();
				instructionsButton();
				creditsButton();
				exitButton();
				break;
			case SCREENS::CREDITS:
			case SCREENS::INSTRUCTIONS:
				back->update();
				if (back->getButtonAction()) {
					currentScreen = SCREENS::MENU;
				}
				break;
			}
		}		
		void draw() {
			switch (currentScreen) {
			case SCREENS::MENU:
				drawMenu();
				break;
			case SCREENS::CREDITS:
				DrawTexture(screenCreditsTexture, 0, 0, WHITE);
				back->draw();
				break;
			case SCREENS::INSTRUCTIONS:
				DrawTexture(screenInstTexture, 0, 0, WHITE);
				back->draw();
				break;
			}
		}
		void deinit() {
			UnloadTexture(screenMenuTexture);
			UnloadTexture(screenInstTexture);
			UnloadTexture(screenCreditsTexture);
			delete play;
			delete instructions;
			delete credits;
			delete exit;
			delete back;
		}
	}
}

