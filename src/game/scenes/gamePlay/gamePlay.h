#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

namespace arkanoid {

	namespace gameplay {		
		void init();
		void update();
		void draw();
		void deinit();
	}
}
#endif
