#include "gamePlay.h"
#include "game/global.h"
#include "game/input/input.h"
#include "game/scenes/menu/menu.h"

using namespace arkanoid;

    paddle* arkanoid::player;
    balloon* arkanoid::ball;
    block* arkanoid::blocks[ROW][COL];
    namespace arkanoid {
        namespace gameplay {
            Texture2D screenLv1Texture;
            Texture2D screenLv2Texture;
            Texture2D screenLv3Texture;
            Texture2D nextLvTexture;
            Texture2D startTexture;
            Texture2D pauseTexture;
            Texture2D winTexture;
            Texture2D life1Texture;
            Texture2D life2Texture;
            Texture2D life3Texture;
            Texture2D gameoverTexture;
            Music     gameplayMusic;
            bool      gameOver;
            short     status;
            float     volume;


            void static spawnBlocks() {
                short space = 5;
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                        blocks[i][j] = new block();
                        if (i == 0) { blocks[i][j]->setPos((blocks[i][j]->getPad().width + space - 1) * j + space, blocks[i][j]->getPad().y); }
                        else { blocks[i][j]->setPos((blocks[i][j]->getPad().width + space - 1) * j + space, blocks[i][j]->getPad().y + (blocks[i][j]->getPad().height + space) * i); }
                        if ((blocks[i][COL - 1] || blocks[i][COL - 2]) && currentScreen == SCREENS::LV_ONE) { blocks[i][j]->setActive(false); }
                    }
                }
            }

            void init() {
                screenLv1Texture = LoadTexture("res/assets/lv_1.png");
                screenLv2Texture = LoadTexture("res/assets/lv_2.png");
                screenLv3Texture = LoadTexture("res/assets/lv_3.png");
                nextLvTexture = LoadTexture("res/assets/next_lv.png");
                startTexture = LoadTexture("res/assets/start.png");
                pauseTexture = LoadTexture("res/assets/pause.png");
                winTexture = LoadTexture("res/assets/win.png");
                life1Texture = LoadTexture("res/assets/life_1.png");
                life2Texture = LoadTexture("res/assets/life_2.png");
                life3Texture = LoadTexture("res/assets/life.png");
                gameoverTexture = LoadTexture("res/assets/gameover.png");

                gameplayMusic= LoadMusicStream("res/fx/gameplay.mp3");
                gameOver = false;
                status = 0;
                volume = 0.2f;

                player = new paddle();
                ball = new balloon();
                spawnBlocks();
            }
            void static activateBlocks() {
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                        blocks[i][j]->setActive(true);
                    }
                }
            }
            void static reboot() {
                StopMusicStream(gameplayMusic);
                currentScreen = SCREENS::MENU;
                input::nextLv = false;
                player->init();
                ball->init();
                activateBlocks();
                gameOver = false;
                input::initGame = false;
                input::pause = false;
            }
            void static back() {
                menu::back->update();
                if (menu::back->getButtonAction()) {                   
                    currentScene = SCENES::MENU;                    
                    reboot();
                }
            }
            void static levelDesign() {
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                        if ((j == COL - 1 || j == 0 || i == ROW - 1 || i == ROW - 2) && currentScreen == SCREENS::LV_ONE) { blocks[i][j]->setActive(false); }
                        if ((j == COL - 5 || j == COL - 4) && currentScreen == SCREENS::LV_TWO) { blocks[i][j]->setActive(false); }
                    }
                }
            }
            void static life() {
                if (ball->getReboot()) {
                    ball->setReboot(false);
                    player->setLife(player->getLife() - 1);
                }
            }
            void static gameover() {
                if (player->getLife() == 0) {
                    gameOver = true;
                    back();
                }
            }
            void static blockStatus() {
                status = 0;
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                        if (!blocks[i][j]->getActive()) { status++; }

                    }
                }
            }
            void static nextLevel() {
                short _lifes = 0;
                blockStatus();
                switch (currentScreen) {
                case SCREENS::LV_ONE:
                    if (status == ROW * COL) {
                        input::initGame = false;
                        back();
                        if (input::nextLv) {
                            _lifes = player->getLife();
                            reboot();
                            player->setLife(_lifes);
                            currentScreen = SCREENS::LV_TWO;
                        }
                    }
                    break;
                case SCREENS::LV_TWO:
                    if (status == ROW * COL) {
                        input::initGame = false;
                        back();
                        if (input::nextLv) {
                            _lifes = player->getLife();
                            reboot();
                            player->setLife(_lifes);
                            currentScreen = SCREENS::LV_THREE;
                        }
                    }
                    break;
                case SCREENS::LV_THREE:
                    if (status == ROW * COL) {
                        input::initGame = false;
                        back();
                    }
                    break;
                }
            }
            void static pause() {
                menu::back->update();
                if (menu::back->getButtonAction()) {
                    currentScene = SCENES::MENU;
                    reboot();
                }
            }
            void static music() {
                UpdateMusicStream(gameplayMusic);
                if (input::mute) { volume = 0; }
                else volume = 0.2f;
                SetMusicVolume(gameplayMusic, volume);
                if (!input::pause) { PlayMusicStream(gameplayMusic);}
                else PauseMusicStream(gameplayMusic);               
            }
            void static collisionPadBall() {
                if (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), player->getPad()) && ball->getSpeed().y > 0) {
                    if (ball->getPos().y - ball->getRadius() < player->getPad().y) {
                        ball->setSpeed(ball->getSpeed().x, ball->getSpeed().y * -1.0f);
                    }
                    else {
                        ball->setSpeed(ball->getSpeed().x * -1.0f, ball->getSpeed().y);
                    }
                }
            }
            void static collisionBlocksBall() {
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                        if (blocks[i][j]->getActive()) {
                            if (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), blocks[i][j]->getPad()) && ball->getSpeed().y > 0) {
                                if (ball->getPos().y - ball->getRadius() < blocks[i][j]->getPad().y) {
                                    ball->setSpeed(ball->getSpeed().x, ball->getSpeed().y * -1.0f);
                                }
                                else {
                                    ball->setSpeed(ball->getSpeed().x * -1.0f, ball->getSpeed().y);
                                }
                                blocks[i][j]->setActive(false);
                                player->setScore(player->getScore() + 1);
                            }
                            else if (CheckCollisionCircleRec(ball->getPos(), ball->getRadius(), blocks[i][j]->getPad()) && ball->getSpeed().y < 0) {
                                if (ball->getPos().y + ball->getRadius() < blocks[i][j]->getPad().y + blocks[i][j]->getPad().width) {
                                    ball->setSpeed(ball->getSpeed().x, ball->getSpeed().y * -1.0f);
                                }
                                else {
                                    ball->setSpeed(ball->getSpeed().x * -1.0f, ball->getSpeed().y);
                                }
                                blocks[i][j]->setActive(false);
                                player->setScore(player->getScore() + 1);
                            }
                        }
                    }
                }
            }
            void static deleteBlocks() {
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                       delete blocks[i][j];
                    }
                }
            }

            void static drawGameover() {
                if (gameOver) {
                    DrawTexture(gameoverTexture, 0, 0, WHITE);
                    menu::back->draw();
                }
            }
            void static drawLife() {
                switch (player->getLife()) {
                case 1:
                    DrawTexture(life1Texture, 0, 0, WHITE);
                    break;
                case 2:
                    DrawTexture(life2Texture, 0, 0, WHITE);
                    break;
                case 3:
                    DrawTexture(life3Texture, 0, 0, WHITE);
                    break;
                }
            }
            void static drawNextLevel() {
                if (status == ROW * COL) {
                    menu::back->draw();
                    if (currentScreen != SCREENS::LV_THREE) {
                        DrawTexture(nextLvTexture, 0, 0, WHITE);
                    }
                    else {
                        DrawTexture(winTexture, 0, 0, WHITE);
                    }
                }
            }
            void static drawBlocks() {
                for (short i = 0; i < ROW; i++) {
                    for (short j = 0; j < COL; j++) {
                        if (blocks[i][j]->getActive()) {
                            blocks[i][j]->draw();
                        }
                    }
                }
            }
            void static drawInit() {
                if (!gameOver) {
                    if (!input::initGame && status < ROW * COL) {
                        DrawTexture(startTexture, 0, 0, WHITE);
                    }
                }
            }
            void static drawPause() {
                if (input::pause) {
                    menu::back->draw();
                    DrawTexture(pauseTexture, 0, 0, WHITE);
                }
            }
            void static drawScore() {
                DrawText(TextFormat("%01i", player->getScore()), screenWidth / 4, static_cast<int>(screenHeight / 22.85f), 40, YELLOW);
            }

            void update() {
                music();
                gameover();
                levelDesign();
                if (!gameOver) {
                    nextLevel();
                    life();
                    if (!input::pause) {
                        player->rebootPad();
                        if (input::initGame) {
                            player->update();
                            ball->update();
                            collisionPadBall();
                            collisionBlocksBall();
                        }
                    }
                    else  pause();
                }
            }
            void draw() {
                switch (currentScreen) {
                case SCREENS::LV_ONE:
                    DrawTexture(screenLv1Texture, 0, 0, WHITE);
                    drawNextLevel();
                    break;
                case SCREENS::LV_TWO:
                    DrawTexture(screenLv2Texture, 0, 0, WHITE);
                    drawNextLevel();
                    break;
                case SCREENS::LV_THREE:
                    DrawTexture(screenLv3Texture, 0, 0, WHITE);
                    drawNextLevel();
                    break;
                }
                drawLife();
                drawInit();
                drawBlocks();
                drawPause();
                drawScore();
                player->draw();
                ball->draw();                
                drawGameover();
            }
            void deinit() {
                UnloadTexture(screenLv1Texture);
                UnloadTexture(screenLv2Texture);
                UnloadTexture(screenLv3Texture);
                UnloadTexture(nextLvTexture);
                UnloadTexture(startTexture);
                UnloadTexture(pauseTexture);
                UnloadTexture(winTexture);
                UnloadTexture(life1Texture);
                UnloadTexture(life2Texture);
                UnloadTexture(life3Texture);
                UnloadTexture(gameoverTexture);
                UnloadMusicStream(gameplayMusic);
                delete player;
                delete ball;
                deleteBlocks();
            }
        }
    }