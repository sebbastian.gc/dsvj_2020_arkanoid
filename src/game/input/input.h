#ifndef INPUT_H
#define INPUT_H

#include "raylib.h"

namespace arkanoid {

	namespace input {
		extern bool left;
		extern bool right;
		extern bool pause;
		extern bool initGame;
		extern bool nextLv;
		extern bool mute;

		void init();
		void update();
	}
}
#endif
