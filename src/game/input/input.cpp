#include "input.h"
#include "game/global.h"

using namespace arkanoid;
using namespace input;

 bool input::left;
 bool input::right;
 bool input::pause;
 bool input::initGame;
 bool input::nextLv;
 bool input::mute;

void input::init() {
	left = false;
	right = false;
	pause = false;
	initGame = false;
	nextLv = false;
	mute = false;
}
void input::update() {
	if (currentScene== SCENES::GAMEPLAY) {
		IsKeyDown(KEY_LEFT) ? left = true : left = false;
		IsKeyDown(KEY_RIGHT) ? right = true : right = false;
		if (IsKeyPressed(KEY_SPACE) && initGame) { !pause ? pause = true : pause = false; }
		if (IsKeyPressed(KEY_ENTER)) { initGame = true; }
		if (IsKeyPressed(KEY_BACKSPACE) && !initGame) { nextLv = true; }
		if (IsKeyPressed(KEY_Q) && !pause) { !mute ? mute = true : mute = false; }

	}	
}

