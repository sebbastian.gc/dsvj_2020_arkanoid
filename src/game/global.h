#ifndef GLOBAL_H
#define GLOBAL_H

#include <iostream>

#include "objects/paddle/paddle.h"
#include "objects/balloon/balloon.h"
#include "objects/block/block.h"

namespace arkanoid {

	extern int screenWidth;
	extern int screenHeight;
	const int ROW = 9;
	const int COL = 8;
	
	enum class SCENES {MENU,GAMEPLAY};
	enum class SCREENS {MENU,CREDITS,INSTRUCTIONS,LV_ONE,LV_TWO,LV_THREE};

	extern paddle* player;	
	extern balloon* ball;
	extern block* blocks[ROW][COL];	
	extern SCENES currentScene;
	extern SCREENS currentScreen;
}
#endif
