#ifndef BLOCK_H
#define BLOCK_H

#include "raylib.h"
namespace arkanoid {

	class block {
	private:
		Texture2D blockTexture;		
		Rectangle pad;		
		Color color;	
		bool active;
	public:
		block();
		~block();
		void init();		
		void setPos(float _x, float _y);
		void setColor(Color _color);	
		void setActive(bool _active);
		Rectangle getPad();
		Color getColor();
		bool getActive();
		void update();
		void draw();
	};
}
#endif