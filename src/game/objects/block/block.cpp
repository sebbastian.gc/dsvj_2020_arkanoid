#include "block.h"
#include "game/global.h"

using namespace arkanoid;

block::block() {
	blockTexture = LoadTexture("res/assets/block.png");
	block::init();
}
block::~block() {
	UnloadTexture(blockTexture);
}
void block::init() {			
	pad.height =30.0f;
	pad.width =70.0f;
	pad.x = 0;
	pad.y = 120;	
	color = RAYWHITE;
	active = true;		
}
void block::setPos(float _x, float _y) {
	pad.x= _x;
	pad.y= _y;	
}
void block::setColor(Color _color) {
	color = _color;
}
void block::setActive(bool _active) {
	active = _active;
}
Rectangle block::getPad() {
	return pad;
}
Color block::getColor() {
	return color;
}
bool block::getActive() {
	return active;
}
void block::update() {
	
}
void block::draw() {
	DrawTexture(blockTexture, static_cast<int>(pad.x), static_cast<int>(pad.y), color);
#if _DEBUG
	DrawRectangleLines(static_cast<int>(pad.x), static_cast<int>(pad.y), static_cast<int>(pad.width), static_cast<int>(pad.height), RED);
#endif 
}