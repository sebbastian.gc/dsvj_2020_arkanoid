#include "paddle.h"
#include "game/global.h"
#include "game/input/input.h"

using namespace arkanoid;

paddle::paddle() {
	padTexture = LoadTexture("res/assets/pad.png");
	paddle::init();
}
paddle::~paddle() {
	UnloadTexture(padTexture);
}
void paddle::init() {
	life = 3;
	score = 0;
	pad.height =31.0f;
	pad.width = 120.0f;
	pad.x = static_cast<float>(arkanoid::screenWidth / 2)-pad.width/2.0f;
	pad.y = static_cast<float>(arkanoid::screenHeight)- (pad.height*2.0f);
	speed = 350.0f;
	color = RAYWHITE;
}
void paddle::setPad(float _x , float _y) {
	pad.x = _x;
	pad.y = _y;
}
void paddle::setColor(Color _color) {
	color = _color;
}
void paddle::setSpeed(float _speed) {
	speed = _speed;
}
void paddle::setScore(short _score) {
	score = _score;
}
void paddle::setLife(short _life) {
	life = _life;
}

Rectangle paddle::getPad() {
	return pad;
}
Color paddle::getColor() {
	return color;
}
float paddle::getSpeed() {
	return speed;
}
short paddle::getScore() {
	return score;
}
short paddle::getLife() {
	return life;
}

void paddle::move() {
	if (input::left && pad.x > 0) {
		pad.x -= speed* GetFrameTime(); ;
	}
	else if (input::right && pad.x + pad.width < static_cast<float>(screenWidth)) {
		pad.x += speed* GetFrameTime(); ;
	}
}
void paddle::rebootPad() {
	if (!input::initGame ) {
		pad.x = static_cast<float>(arkanoid::screenWidth / 2) - pad.width / 2.0f;
	}
}
void paddle::update() {
	paddle::move();	
}
void paddle::draw() {	
	DrawTexture(padTexture, static_cast<int>(pad.x), static_cast<int>(pad.y), color);
	#if _DEBUG
	DrawRectangleLines(static_cast<int>(pad.x), static_cast<int>(pad.y), static_cast<int>(pad.width), static_cast<int>(pad.height), GREEN);
	#endif 

	

}