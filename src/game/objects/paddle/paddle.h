#ifndef PADDLE_H
#define PADDLE_H

#include "raylib.h"
namespace arkanoid {

	class paddle {
	private:
		Texture2D padTexture;
		Rectangle pad;
		Color color;
		short score;
		float speed;
		short  life;
	public:
		paddle();
		~paddle();

		void init();
		void setPad(float _x, float _y);
		void setColor(Color _color);
		void setSpeed(float _speed);
		void setScore(short _score);
		void setLife(short _life);

		Rectangle getPad();
		Color getColor();
		float getSpeed();
		short getScore();
		short getLife();

		void move();
		void rebootPad();
		void update();
		void draw();
	};
}
#endif

