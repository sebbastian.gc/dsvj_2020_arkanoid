#include "balloon.h"
#include "game/global.h"
#include "game/input/input.h"

using namespace arkanoid;

balloon::balloon() {
	ballTexture = LoadTexture("res/assets/ball.png");
	balloon::init();
}
balloon::~balloon() {
	UnloadTexture(ballTexture);
}
void balloon::init() {
	reboot=false;
	pos.x = static_cast<float>(screenWidth/2);
	pos.y = static_cast<float>(screenHeight/1.1f);
	speed = { 350.0f, 350.0f };
	radius = 8.0f;
	collision = false;	
	color = RAYWHITE;
}
void balloon::setPos(float _x,float _y) {
	pos.x = _x;
	pos.y = _y;
}
void balloon::setSpeed(float _x, float _y) {
	speed = { _x, _y };
}
void balloon::setRadius(float _radius) {
	radius = _radius;
}
void balloon::setCollision(bool _collision) {
	collision = _collision;
}
void balloon::setColor(Color _color) {
	color = _color;
}
void balloon::setReboot(bool _reboot) {
	reboot = _reboot;
}

Vector2 balloon::getPos() {
	return pos;
}
Vector2 balloon::getSpeed() {
	return speed;
}
float balloon::getRadius() {
	return radius;
}
bool balloon::getCollision() {
	return collision;
}
Color balloon::getColor() {
	return color;
}
bool balloon::getReboot() {
	return reboot;
}

void balloon::collisionWalls() {
	if ((pos.x >= (static_cast<float>(screenWidth) - radius)) || (pos.x <= radius)) {
		speed.x *= -1.0f;
	}
	else if (pos.y <= static_cast<float>(screenHeight/8)&&speed.y<0) {
		speed.y *= -1.0f;
	}
}
void balloon::move() {
	pos.y += speed.y* GetFrameTime(); ;
	pos.x += speed.x* GetFrameTime(); ;
	balloon::collisionWalls();
}
void balloon::rebootBall() {
	if (pos.y > static_cast<float>(screenHeight) - radius) {
		pos.x = static_cast<float>(screenWidth / 2);
		pos.y = static_cast<float>(screenHeight / 1.1f);
		speed.x *= 1.0f;
		input::initGame = false;
		reboot = true;
	}
}
void balloon::update() {
	balloon::move();
	balloon::rebootBall();
}
void balloon::draw() {	
	DrawTexture(ballTexture, static_cast<int>(pos.x-radius), static_cast<int>(pos.y-radius), color);
#if _DEBUG
	DrawCircleLines(static_cast<int>(pos.x), static_cast<int>(pos.y), radius, GREEN);
#endif 
}

