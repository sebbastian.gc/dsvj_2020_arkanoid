#ifndef BALLOON_H
#define BALLOON_H

#include "raylib.h"
namespace arkanoid {

	class balloon {
	private:
		Texture2D ballTexture;
		Vector2 pos;
		Vector2 speed;
		bool reboot;
		float radius;
		bool collision;		
		Color color;
	public:
		balloon();
		~balloon();
		void init();
		void setPos(float _x, float _y);
		void setSpeed(float _x, float _y);
		void setRadius(float _radius);
		void setCollision(bool _collision);		
		void setColor(Color _color);
		void setReboot(bool _reboot);

		Vector2 getPos();
		Vector2 getSpeed();
		float getRadius();
		bool getCollision();		
		Color getColor();
		bool getReboot();

		void collisionWalls();
		void rebootBall();
		void move();		
		void update();
		void draw();
	};
}
#endif
