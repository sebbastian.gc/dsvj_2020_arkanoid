#ifndef GAMELOOP_H
#define GAMELOOP_H

#include "raylib.h"

namespace arkanoid {

	namespace gameLoop {		
		void run();		
	}

}
#endif
