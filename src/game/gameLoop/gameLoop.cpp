#include "gameLoop.h"

#include "game/global.h"
#include "game/input/input.h"
#include "game/scenes/menu/menu.h"
#include "game/scenes/gamePlay/gamePlay.h"

using namespace arkanoid;

int arkanoid::screenWidth;
int arkanoid::screenHeight;
SCENES arkanoid::currentScene;
namespace arkanoid {
    namespace gameLoop {

        void static init() {
            screenWidth = 600;
            screenHeight = 800;
            InitWindow(screenWidth, screenHeight, " ");
            SetTargetFPS(60);
            InitAudioDevice();
            input::init();
            menu::init();
            gameplay::init();

        }
        void static input() {
            input::update();
        }
        void static update() {
            switch (currentScene) {
            case SCENES::MENU:
                menu::update();
                break;
            case SCENES::GAMEPLAY:
                gameplay::update();
                break;
            }
        }
        void static draw() {
            BeginDrawing();
            ClearBackground(BLACK);
            switch (currentScene) {
            case SCENES::MENU:
                menu::draw();
                break;
            case SCENES::GAMEPLAY:
                gameplay::draw();
                break;
            }
            EndDrawing();
        }
        void static deinit() {
            menu::deinit();
            gameplay::deinit();
            CloseAudioDevice();
        }
        void run() {
            init();
            while (!WindowShouldClose()) {
                input();
                update();
                draw();
            }
            deinit();
        }
       
    }
}
